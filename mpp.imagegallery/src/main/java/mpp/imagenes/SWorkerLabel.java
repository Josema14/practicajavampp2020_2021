package mpp.imagenes;

import java.io.IOException;

import java.util.LinkedList;
import java.util.List;

import javax.swing.SwingWorker;

import mpp.imagenes.db.LabelledImage;
import mpp.imagenes.labels.LabelDetector;
import mpp.imagenes.labels.LabelDetector.Label;

public class SWorkerLabel extends SwingWorker<Integer, Integer> {

	private List<LabelledImage> img;
	private LabelDetector labelDetector;
	private List<List<Label>> listaLabels = new LinkedList<List<Label>>();
	private Controller c;
	private int size = 0;

	public SWorkerLabel(List<LabelledImage> img, LabelDetector l, Controller c) {
		this.img = img;
		this.labelDetector = l;
		this.c = c;
		this.size = img.size();
	}

	@Override
	protected Integer doInBackground() throws Exception {
		int j = 0;
		try {

			// Recorremos con el size especificado para evitar problemas si se ha a�adido
			// alguna imagen
			for (j = 0; j < size; j++) {
				if (isCancelled())
					return null;
				List<Label> labels = labelDetector.label(img.get(j).getFile());
				listaLabels.add(labels);
				// Publicamos que hemos obtenido una serie de labels
				publish(j);

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return j;
	}

	// Actualizamos la barra y a�adimos las etiquetas calculadas
	@Override
	protected void process(List<Integer> valores) {

		if (!isCancelled()) {

			int valor = valores.get(valores.size() - 1);

			for (int i = valores.get(0); i <= valor; i++) {
				LabelledImage imagen = img.get(i);
				for (Label label : listaLabels.get(valor)) {
					imagen.addLabel(label.getValue());
				}
				c.actualizarProgreso(this);
			}

		}
	}

}
