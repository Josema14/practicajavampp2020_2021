package mpp.imagenes;

import java.awt.Image;
import java.io.File;
import java.util.LinkedList;
import java.util.List;


import javax.swing.SwingWorker;

public class SWorkerLoadImages extends SwingWorker<List<Image>, Integer> {

	private List<File> f;
	private int height;
	private int width;
	private Controller c;
	private LinkedList<Image> imagenes = new LinkedList<Image>();

	// Funcin para crear el SwingWorker

	// Necesitaremos el fichero, la altura, la anchura y el controlador
	public SWorkerLoadImages(List<File> f, int height, int width, Controller c) {
		this.f = f;
		this.height = height;
		this.width = width;
		this.c = c;
	}

	@Override
	protected LinkedList<Image> doInBackground() throws Exception {

		// Si falla nos devolver null, que analizaremos en el controlador al indicar la
		// barra de progreso

		int contador = 0;
		// Recorremos la lista y vamos cargando las imgenes
		for (File file : f) {

			if (isCancelled())
				return null;
			Image image = c.loadScaled(file, width, height);
			imagenes.add(image);
			publish(contador);
			contador++;
		}

		return imagenes;
	}


	//Actualizamos la barra y aadimos las imagenes 
	@Override
	protected void process(List<Integer> cargadas) {
		while (!isCancelled()) {
			int posicion = cargadas.get(cargadas.size() - 1);
			
			for (int i = cargadas.get(0);i<=posicion;i++) {
			
				c.cargarImagenIndividual(f.get(i), imagenes.get(i),this);	
			}
			break;
			
		}
	}

}


