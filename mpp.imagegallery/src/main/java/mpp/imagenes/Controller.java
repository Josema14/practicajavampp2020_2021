package mpp.imagenes;

import java.awt.Image;
import java.awt.color.ColorSpace;
import java.awt.color.ICC_Profile;
import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.imageio.ImageIO;
import javax.swing.SwingWorker;

import mpp.imagenes.db.ImageDB;
import mpp.imagenes.db.LabelledImage;
import mpp.imagenes.labels.LabelDetector;
import mpp.imagenes.labels.MockLabelDetector;

/**
 * 
 * @author jesus
 *
 */

public class Controller {

	@Nonnull
	private final ImageDB db;
	@Nonnull
	private final ImageManager view;
	@Nonnull
	private final LabelDetector labelDetector = new MockLabelDetector();
	@CheckForNull
	private LabelledImage currentImage;

	private List<SwingWorker> listaHilos = new ArrayList<>();

	private final static int NUM_HILOS = Runtime.getRuntime().availableProcessors();

	private ExecutorService threadPool = Executors.newFixedThreadPool(NUM_HILOS);
	private ExecutorService threadPoolSingle = Executors.newSingleThreadExecutor();

	static {
		// Will in turn force invocation of ProfileDeferralMgr.activateProfiles()
		ICC_Profile.getInstance(ColorSpace.CS_sRGB).getData();
	}

	// Contador de imagenes cargando simultaneamente

	private int contadorTareas = 0;
	private int numTareas = 0;

	public Controller(@Nonnull ImageManager view) {
		this.view = view;
		this.db = new ImageDB();
	}

	public LabelledImage setCurrentImage(File file) {
		LabelledImage img = db.getImage(file);
		if (img == null)
			return null;
		this.currentImage = img;
		return img;
	}

	public LabelledImage getCurrentImage() {
		return currentImage;
	}

	public void loadScaledImage(@Nonnull File f, @Nonnegative int width, @Nonnegative int height) throws IOException {
		numTareas++;
		SWorkerLoad hilo = new SWorkerLoad(f, width, height, this);
		hilo.execute();

	}

	public void cargarImagenIndividual(File f, Image i, SwingWorker s) {

		// Dependiendo de si la imagen ha podido cargar o no, la aadiremos a la base de
		// datos
		if (i != null && !s.isCancelled()) {
			db.addImage(f);
			view.addThumbnail(i, f);

		}

		actualizarProgreso(s);
	}

	// Funcin para actualizar la barra de progreso
	public void actualizarProgreso(SwingWorker s) {

		contadorTareas++;
		if (contadorTareas == numTareas) {

			view.setInfo("Tareas terminadas", 100);
			contadorTareas = 0;
			numTareas = 0;
		} else {

			if (numTareas != 0)
				view.setInfo("Realizando tareas: " + (numTareas - contadorTareas) + " restantes.",
						contadorTareas * 100 / numTareas);

		}

		if (s.isCancelled() || s.isDone())
			listaHilos.remove(s);

	}

	public void loadScaledImages(@Nonnull List<? extends File> files, @Nonnegative int width, @Nonnegative int height)
			throws IOException {
		// Aumentamos el nmero de tareas y ejecutamos el hilo

		numTareas += files.size();

		SWorkerLoadImages hilo = new SWorkerLoadImages((List<File>) files, width, height, this);
		listaHilos.add(hilo);
		threadPool.execute(hilo);

	}

	// http://www.java2s.com/Code/Java/2D-Graphics-GUI/ImageFilter.htm
	public void transformAll(@Nonnull File folder) {
		if (!db.getImages().isEmpty()) {

			numTareas += db.getImages().size();
			for (LabelledImage labelledImage : db.getImages()) {
				SWorkerTransform hilo = new SWorkerTransform(folder, labelledImage, this);
				listaHilos.add(hilo);
				threadPool.execute(hilo);
			}

		}

		else
			view.setInfo("No hay imgenes en la base de datos", 0);

	}

	@CheckForNull
	protected Image loadScaled(@Nonnull File f, @Nonnegative int w, @Nonnegative int h) throws IOException {
		System.out.println("Processing: " + f);
		BufferedImage img = ImageIO.read(f);
		if (img == null)
			return null;
		return img.getScaledInstance(w, h, Image.SCALE_FAST);
	}

	public void stopBackgroundTasks() {
		// Esta funcin parar todos los hilos

		for (SwingWorker swingWorker : listaHilos) {
			swingWorker.cancel(true);
		}

		// Limpiamos lista hilos, actualizamos el contador y cambiamos la barra de
		// progreso
		listaHilos.clear();
		contadorTareas = 0;
		numTareas = 0;
		view.setInfo("Se ha detenido la carga", 0);

	}

	// Funcin para realizar el etiquetado de forma asncrona
	public void autoLabel() {

		if (!db.getImages().isEmpty()) {
			view.setInfo("Empezando a etiquetar", 0);
			List<LabelledImage> list = (List<LabelledImage>) db.getImages();
			SWorkerLabel hilo = new SWorkerLabel(list, labelDetector, this);
			numTareas += list.size();

			listaHilos.add(hilo);
			threadPoolSingle.execute(hilo);
		}

		else
			view.setInfo("No hay imgenes en la base de datos", 0);
	}

}
