package mpp.imagenes;


import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;


import javax.imageio.ImageIO;
import javax.swing.SwingWorker;

import mpp.imagenes.db.LabelledImage;

public class SWorkerTransform extends SwingWorker<Boolean,Void> {
	
	
	private File folder;
	LabelledImage imagen;
	private Controller c;
	
	//Funcin para crear el SwingWorker
	
	//Necesitaremos el fichero, la altura, la anchura y el controlador
	public SWorkerTransform(File f,LabelledImage imagen, Controller c) {
		this.folder = f;
		this.imagen = imagen;
		this.c = c;
	}

	@Override
	protected Boolean doInBackground() throws Exception {
		
		//Funcion para transformar una imagen
		while(!isCancelled()) {
	
			File f = imagen.getFile();
			System.out.println("Transforming: " + f);
			try {
				BufferedImage image = ImageIO.read(f);
				float[] blurMatrix = { 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f,
						1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f };
				BufferedImageOp blurFilter = new ConvolveOp(new Kernel(3, 3, blurMatrix), ConvolveOp.EDGE_NO_OP, null);

				BufferedImage filtered = blurFilter.filter(image, null);

				File outputFileName = Paths.get(folder.getPath(), f.getName()).toFile();
				ImageIO.write(filtered, "jpg", outputFileName);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return true;
		
		
	}
		 return false;
	}
	
	@Override
	protected void done() {
		
	
	
			//Informamos de que se ha realizado la transformacin
			if (!isCancelled()) {
			c.actualizarProgreso(this);
			}
		
		
	}

}
