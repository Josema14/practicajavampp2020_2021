package mpp.imagenes;

import java.awt.Image;
import java.io.File;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

public class SWorkerLoad extends SwingWorker<Image,Void> {
	
	private File f;
	private int height;
	private int width;
	private Controller c;
	
	//Funcin para crear el SwingWorker
	
	//Necesitaremos el fichero, la altura, la anchura y el controlador
	public SWorkerLoad(File f, int height, int width, Controller c) {
		this.f = f;
		this.height = height;
		this.width = width;
		this.c = c;
	}

	@Override
	protected Image doInBackground() throws Exception {
		
		//Si falla nos devolver null, que analizaremos en el controlador al indicar la barra de progreso
	
		Image image = c.loadScaled(f, width, height);
		
		
		
		
		return image;
	}
	
	
	protected void done() {
		
	
		
		try {
			//Enviaremos la imagen, si es nula, la analizar el controlador.
			c.cargarImagenIndividual(f, get(),this);
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	

}
